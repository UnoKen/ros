#include "ros/ros.h"
#include "std_msgs/String.h"
#include <magnetic_foot_msgs/magnet_feet_cmd.h>
#include <magnetic_foot_msgs/magnet_feet_feedback.h>

#include <sstream>

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{

  ros::init(argc, argv, "magnetic_feet_manual_ope");
  ros::NodeHandle nh;

  ros::Publisher magnetic_feet_pub = nh.advertise<magnetic_foot_msgs::magnet_feet_cmd>("magnetic_feet", 1000);
  sleep(1);  // waiting for to link to the subscriber
  ROS_INFO("Publisher is set.");

  magnetic_foot_msgs::magnet_feet_cmd cmd_msg;
	int number;
  int foot = 0;
  int reversed_polarity = 0;
  int degaussing_time = 0;

  while (1)
  {
    // input foot number
    std::cout << "foot number?( 0:LF, 1:RF, 2:LH, 3:RH) \n>> ";
    std::cin >> foot;
    if ( std::cin.fail() || foot < 0 || foot >= 4)
    {
      std::cout << "foot number should be 0 -- 3. \n" << std::endl;
      std::cin.clear();            // flash cin function
      std::cin.ignore(1024, '\n'); // flash cin function
      continue;
    }
    // input direction of polarity
    std::cout << "direction of polarity? ( 0: degaussing, 1: reversed(double magnetizing) ) \n>> ";
    std::cin >> reversed_polarity;
    if ( reversed_polarity != 0 && reversed_polarity != 1 )
    {
      std::cout << "direction of polarity should be 0 or 1. \n" << std::endl;
      std::cin.clear();            // flash cin function
      std::cin.ignore(1024, '\n'); // flash cin function
      continue;
    }
    // input holding time
    std::cout << "holding time [msec]? ( maximum: 6000 ) \n>> ";
    std::cin >> degaussing_time;
    if ( std::cin.fail() || degaussing_time < 0 )
    {
      std::cout << "holding time should be positive integer. \n" << std::endl;
      std::cin.clear();            // flash cin function
      std::cin.ignore(1024, '\n'); // flash cin function
      continue;
    }
    break;
  }

  cmd_msg.foot = foot;
  cmd_msg.reversed_polarity = reversed_polarity;
  cmd_msg.degaussing_time = degaussing_time;

  magnetic_feet_pub.publish(cmd_msg);
            ROS_INFO("magnet_feet was published!!");
  sleep(1); // waiting for publishing

  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}

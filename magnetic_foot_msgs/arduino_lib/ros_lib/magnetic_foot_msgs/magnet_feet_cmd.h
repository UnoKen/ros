#ifndef _ROS_magnetic_foot_msgs_magnet_feet_cmd_h
#define _ROS_magnetic_foot_msgs_magnet_feet_cmd_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace magnetic_foot_msgs
{

  class magnet_feet_cmd : public ros::Msg
  {
    public:
      typedef int8_t _foot_type;
      _foot_type foot;
      typedef int16_t _degaussing_time_type;
      _degaussing_time_type degaussing_time;
      typedef bool _reversed_polarity_type;
      _reversed_polarity_type reversed_polarity;

    magnet_feet_cmd():
      foot(0),
      degaussing_time(0),
      reversed_polarity(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_foot;
      u_foot.real = this->foot;
      *(outbuffer + offset + 0) = (u_foot.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->foot);
      union {
        int16_t real;
        uint16_t base;
      } u_degaussing_time;
      u_degaussing_time.real = this->degaussing_time;
      *(outbuffer + offset + 0) = (u_degaussing_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_degaussing_time.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->degaussing_time);
      union {
        bool real;
        uint8_t base;
      } u_reversed_polarity;
      u_reversed_polarity.real = this->reversed_polarity;
      *(outbuffer + offset + 0) = (u_reversed_polarity.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->reversed_polarity);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_foot;
      u_foot.base = 0;
      u_foot.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->foot = u_foot.real;
      offset += sizeof(this->foot);
      union {
        int16_t real;
        uint16_t base;
      } u_degaussing_time;
      u_degaussing_time.base = 0;
      u_degaussing_time.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_degaussing_time.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->degaussing_time = u_degaussing_time.real;
      offset += sizeof(this->degaussing_time);
      union {
        bool real;
        uint8_t base;
      } u_reversed_polarity;
      u_reversed_polarity.base = 0;
      u_reversed_polarity.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->reversed_polarity = u_reversed_polarity.real;
      offset += sizeof(this->reversed_polarity);
     return offset;
    }

    const char * getType(){ return "magnetic_foot_msgs/magnet_feet_cmd"; };
    const char * getMD5(){ return "09909b1b230a8177f60b8a550809331c"; };

  };

}
#endif

#ifndef _ROS_magnetic_foot_msgs_magnet_feet_feedback_h
#define _ROS_magnetic_foot_msgs_magnet_feet_feedback_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace magnetic_foot_msgs
{

  class magnet_feet_feedback : public ros::Msg
  {
    public:
      typedef int8_t _feedback_type;
      _feedback_type feedback;

    magnet_feet_feedback():
      feedback(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_feedback;
      u_feedback.real = this->feedback;
      *(outbuffer + offset + 0) = (u_feedback.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->feedback);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_feedback;
      u_feedback.base = 0;
      u_feedback.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->feedback = u_feedback.real;
      offset += sizeof(this->feedback);
     return offset;
    }

    const char * getType(){ return "magnetic_foot_msgs/magnet_feet_feedback"; };
    const char * getMD5(){ return "2c6e689ebd71476d42e31865c854d31d"; };

  };

}
#endif
